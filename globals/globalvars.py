version = "beta-6d"
appDBVersion = 0.50

videoRoot = "/var/www/"

# Global oAuth Dictionary
oAuthProviderObjects = {}

# Current Theme Settings Data
themeData = {}

# In-Memory Cache of Topic Names
topicCache = {}

# Create In-Memory Invite Cache to Prevent High CPU Usage for Polling Channel Permissions during Streams
inviteCache = {}

# Build Channel Restream Subprocess Dictionary
restreamSubprocesses = {}

# Build Edge Restream Subprocess Dictionary
activeEdgeNodes = []
edgeRestreamSubprocesses = {}
